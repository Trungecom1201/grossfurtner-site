<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'grossfur_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ue2cevxwt1mivq5n2watac0hlbdfjvr7ic2y4v0la8zbn1lydhilzxeiplflqebw' );
define( 'SECURE_AUTH_KEY',  'pscbwxpmcbwlfwwmt5jobouefr70zbqqjwmkavme1gi3qy3p4bdzaxxesu55wiow' );
define( 'LOGGED_IN_KEY',    'hzhinqecp8wz9652or0hly6pezle0tavmho8iv1dgxfhalvnepldaennb6c4xvd5' );
define( 'NONCE_KEY',        'tdwhtualryzbhigrdtv9eoxxh8qkt5884c01n0qrpu4fy1jpgbx4jh8jkpf2ovwu' );
define( 'AUTH_SALT',        'qszuz0gec78i4rcauuwqbmgjdratzbtodx15isk6hinxvqh8wmvpkuopa9shnrla' );
define( 'SECURE_AUTH_SALT', 'mdaumwc1zogeicqs9ukissev12lmoohhhnqletvcwfwr0ymznjzurgfcg69uw6df' );
define( 'LOGGED_IN_SALT',   'j6cglyvd6dhogew24wv9eymesdjtfhbpiuusv4mg0dbwh9sl1wv8zpxzh0rrdiow' );
define( 'NONCE_SALT',       '3bqehhjljqezlizjvylrwvsv8dwclghdeuty34bti1la1hj2hrh64zdkmjwgozhd' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpak_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('WP_MEMORY_LIMIT', '256M');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
